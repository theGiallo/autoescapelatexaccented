# -*- coding: utf-8 -*-
#
#  autoescapelatexaccented.py - AutoEscapeLaTeXAccented: Auto inserts escaped LaTeX accented characters
#
#  author: Gianluca "theGiallo" Alloisio
#  mail: gianluca.alloisio@gmail.com
#
#  based on bracketcompletion.py - Bracket completion plugin for gedit Copyright (C) 2006 - Steve Frécinaux
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330,
#  Boston, MA 02111-1307, USA.

from gi.repository import GObject, Gtk, Gdk, Gedit

accented_characters_escape = {
    u'À' : '\\`A',
    u'Á' : '\\\'A',
    u'È' : '\\`E',
    u'É' : '\\\'E',
    u'Ì' : '\\`I',
    u'Í' : '\\\'I',
    u'Ò' : '\\`O',
    u'Ó' : '\\\'O',
    u'Ù' : '\\`U',
    u'Ú' : '\\\'U',
    u'à' : '\\`a',
    u'á' : '\\\'a',
    u'è' : '\\`e',
    u'é' : '\\\'e',
    u'ì' : '\\`{i}',
    u'í' : '\\\'{i}',
    u'ò' : '\\`o',
    u'ó' : '\\\'o',
    u'ù' : '\\`u',
    u'ú' : '\\\'u',
}
accented_characters = [
    u'À',
    u'Á',
    u'È',
    u'É',
    u'Ì',
    u'Í',
    u'Ó',
    u'Ò',
    u'Ù',
    u'Ú',
    u'á',
    u'à',
    u'è',
    u'é',
    u'ì',
    u'í',
    u'ó',
    u'ò',
    u'ù',
    u'ú',
]


class AutoEscapeLatexAccentedPlugin(GObject.Object, Gedit.ViewActivatable):
    __gtype_name__ = "AutoEscapeLaTeXAccented"

    view = GObject.property(type=Gedit.View)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self._doc = self.view.get_buffer()
        self._last_iter = None
        self._relocate_marks = True
        self.update_language()

        self._handlers = [
            None,
            None,
            self.view.connect('notify::editable', self.on_notify_editable),
            self._doc.connect('notify::language', self.on_notify_language),
            None,
        ]
        self.update_active()

    def do_deactivate(self):
        if self._handlers[0]:
            self.view.disconnect(self._handlers[0])

    def update_active(self):
        # Don't activate the feature if the buffer isn't editable
        active = self.view.get_editable()

        if active and self._handlers[0] is None:
            self._handlers[0] = self.view.connect('key-press-event',
                                                   self.on_key_press_event)
        elif not active and self._handlers[0] is not None:
            self.view.disconnect(self._handlers[0])
            self._handlers[0] = None

    def update_language(self):
        lang = self._doc.get_language()
        if lang is None:
            self.active = False;
            return

        lang_id = lang.get_id()
        if lang_id == 'latex':
            self.active = True;
        else:
            self.active = False;

    def on_notify_language(self, view, pspec):
        self.update_language()
        self.update_active()

    def on_notify_editable(self, view, pspec):
        self.update_active()
    
    def on_key_press_event(self, view, event):
        if event.type != Gdk.EventType.KEY_PRESS or \
           event.state & (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD1_MASK) or \
	   unichr(Gdk.keyval_to_unicode(event.keyval)) not in accented_characters or \
           not self.active:	
        	return False
	key = unichr(Gdk.keyval_to_unicode(event.keyval))
        escaped = accented_characters_escape[key]
	currentiter = self._doc.get_iter_at_mark(self._doc.get_insert ());
        self._doc.begin_user_action()
	self._doc.insert(currentiter, escaped);
        self._doc.end_user_action()
	return True

# ex:ts=4:et:
