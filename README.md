# AutoEscapeLaTeXAccented, a plugin for gedit #

Automatically inserts escaped LaTeX accented characters.
<https://bitbucket.org/theGiallo/autoescapelatexaccented>
v1.0

All bug reports / feature requests / miscellaneous comments are welcome
at <https://bitbucket.org/theGiallo/autoescapelatexaccented/issues>.

## Usage ##

*   Open a LaTeX file and write!!

## Requirements ##

I've written it on Gedit 3.2.4, it's not tested on others versions, please report

## Credits ##

Code based on [Bracket completion plugin for gedit] by Steve Frécinaux

## License ##

Copyright &copy; 2010-2011 Gianluca "theGiallo" Alloisio <gianluca.alloisio@gmail.com>

Available under GNU General Public License version 3


[Bracket completion plugin for gedit]: http://src.gnu-darwin.org/ports/editors/gedit-plugins/work/gedit-plugins-2.20.0/plugins/bracketcompletion/bracketcompletion.py
